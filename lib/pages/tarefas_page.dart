import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';
import 'package:todo/utils/ordenacao.dart';
import 'package:todo/utils/prefs.dart';
import 'package:todo/widgets/constrained_box.dart';
import 'package:todo/widgets/dialod.dart';
import 'package:todo/widgets/expansion_tile.dart';
import 'package:todo/widgets/floating_button.dart';
import 'package:todo/widgets/lisi_tile.dart';
import 'package:todo/widgets/menus/menu.dart';
import 'package:todo/widgets/menus/menu_de_cores.dart';
import 'package:todo/widgets/text_form_field.dart';
import 'conteudos_page.dart';

class TarefasPage extends StatefulWidget {
  @override
  _TarefasPageState createState() => _TarefasPageState();
}

TextEditingController _tarefaController = TextEditingController();
TextEditingController _pesquisaController = TextEditingController();
List _tarefas = [];
bool _mostrarConteudosConcluidos = true,
    _pesquisa = false,
    _mostrarNumerosDaTarefa = true;
List _resultadoPesquisa = [];
String ordemConteudos = 'crescente';
String ordemTarefas = 'crescente';

class _TarefasPageState extends State<TarefasPage> {
  @override
  void initState() {
    super.initState();
    setState(() {
      getCor();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: !_pesquisa
          ? floatingButton(
              controller: _tarefaController,
              context: context,
              dialogName: 'Adicionar uma tarefa',
              confirm: () => _confirmActionAddtarefa(),
              cancel: () => _tarefaController.text = '')
          : Container(),
      appBar: AppBar(
        backgroundColor: colorMain,
        actions: actions(),
        title: Text('Tarefas'),
      ),
      drawer: !_pesquisa ? SafeArea(child: Drawer(child: drawer())) : null,
      body: FutureBuilder(
        future: Prefs.getString('tarefas'),
        builder: (context, snapshot) {
          try {
            _tarefas = json.decode(snapshot.data);
          } catch (e) {}
          if (_tarefas.length == 0) {
            return Center(
              child: Text(
                'Nenhuma tarefa foi adicionada',
                style: TextStyle(fontSize: 20),
              ),
            );
          }
          return ListView.builder(
            padding: EdgeInsets.all(10),
            itemCount: !_pesquisa ? _tarefas.length : _resultadoPesquisa.length,
            itemBuilder: (context, index) {
              return ListTile(
                trailing: valoresTarefas(index),
                title: !_pesquisa
                    ? Text('${_tarefas[index]['tarefa']}')
                    : Text('${_resultadoPesquisa[index]['tarefa']}'),
                onTap: () async {
                  int idx = 0;
                  if (_pesquisa) {
                    for (int i = 0; i < _tarefas.length; i++) {
                      if (_tarefas[i]['idTarefa'] ==
                          _resultadoPesquisa[index]['idTarefa']) {
                        idx = i;
                        break;
                      }
                    }
                  }
                  await push(
                      context,
                      ConteudoPage(
                        tarefas: _tarefas,
                        tarefa: !_pesquisa ? _tarefas[index] : _tarefas[idx],
                        idTarefa: !_pesquisa ? index : idx,
                        conteudosMostrar: _mostrarConteudosConcluidos,
                      ));
                  setState(() {
                    _tarefas = _tarefas;
                  });
                },
                onLongPress: () {
                  Menu().showOptions(
                    context,
                    1,
                    children: [
                      constrainedBox(
                          text: 'Editar',
                          onPressed: () {
                            Navigator.pop(context);
                            _editarTarefa(
                                index: index,
                                tarefas: _tarefas,
                                idTarefa: !_pesquisa
                                    ? _tarefas[index]['idTarefa']
                                    : _resultadoPesquisa[index]['idTarefa']);
                          }),
                      constrainedBox(
                          text: 'Apagar',
                          onPressed: () {
                            Navigator.pop(context);
                            _delettarefa(
                                index: index,
                                tarefas: _tarefas,
                                idTarefa: !_pesquisa
                                    ? _tarefas[index]['idTarefa']
                                    : _resultadoPesquisa[index]['idTarefa']);
                          }),
                    ],
                  );
                },
              );
            },
          );
        },
      ),
    );
  }

  valoresTarefas(int index) {
    if (_tarefas[index]['total'] > 0 && _mostrarNumerosDaTarefa) {
      return Text(
          '${_tarefas[index]['concluidas']}/${_tarefas[index]['total']}');
    } else {
      return null;
    }
  }

  getCor() async {
    ordemTarefas = await Prefs.getString('ordenacao');
    ordemConteudos = await Prefs.getString('ordenacaoConteudo');
    String mostrarNumerosDaTarefa =
        await Prefs.getString('mostrarNumerosDaTarefa');
    if (mostrarNumerosDaTarefa == '') {
      Prefs.setString('mostrarNumerosDaTarefa', 'mostrar');
    } else if (mostrarNumerosDaTarefa == 'mostrar') {
      _mostrarNumerosDaTarefa = true;
    } else {
      _mostrarNumerosDaTarefa = false;
    }
    if (ordemConteudos == '') {
      Prefs.setString('ordenacaoConteudo', 'crescente');
      ordemConteudos = 'crescente';
    }
    if (ordemTarefas == '') {
      Prefs.setString('ordenacao', 'crescente');
      ordemTarefas = 'crescente';
    }
    String mostrarConteudosConcluidos =
        await Prefs.getString('mostrarConteudosConcluidos');
    Color cor;
    switch (mostrarConteudosConcluidos) {
      case 'mostrar':
        _mostrarConteudosConcluidos = true;
        break;
      case 'ocultar':
        _mostrarConteudosConcluidos = false;
        break;
      default:
        _mostrarConteudosConcluidos = true;
    }
    switch (await Prefs.getString('cor')) {
      case 'azul':
        cor = Colors.blue;
        break;
      case 'laranja':
        cor = Colors.deepOrangeAccent;
        break;
      case 'amarelo':
        cor = Colors.amber;
        break;
      case 'vermelho':
        cor = Colors.red;
        break;
      case 'marrom':
        cor = Colors.brown;
        break;
      case 'preto':
        cor = Colors.black;
        break;
      case 'roxo':
        cor = Colors.purple;
        break;
      case 'verde':
        cor = Colors.green;
        break;
      default:
        cor = Colors.blue;
    }
    setState(() {
      colorMain = cor;
    });
  }

  _confirmActionAddtarefa() async {
    if (_tarefaController.text != '') {
      List tarefas = _tarefas;
      int id = new DateTime.now().millisecondsSinceEpoch;

      tarefas.add({
        "idTarefa": id,
        "concluidas": 0,
        "total": 0,
        "tarefa": "${_tarefaController.text}",
        "conteudo": []
      });
      String ordem = await Prefs.getString('ordenacao');
      tarefas = ordena(
          list: tarefas,
          key: 'tarefa',
          reverse: ordem == 'crescente' ? false : true);

      setState(() {
        // tarefas.sort((a, b) => a['tarefa'].compareTo(b['tarefa']));
        Prefs.setString('tarefas', "${json.encode(tarefas)}");
      });
      _tarefaController.text = '';
    }
  }

  _delettarefa({tarefas, idTarefa, index}) {
    showInfo(
      label: 'Deletar esta tarefa',
      context: context,
      textOK: 'Deletar',
      conformAction: () {
        if (_pesquisa) {
          for (int i = 0; i < tarefas.length; i++) {
            if (tarefas[i]['idTarefa'] == idTarefa) {
              setState(() {
                tarefas.removeAt(i);
                _resultadoPesquisa.removeAt(index);
                Prefs.setString('tarefas', json.encode(tarefas));
              });
              break;
            }
          }
        } else {
          setState(() {
            tarefas.removeAt(index);
            _resultadoPesquisa = tarefas;
            Prefs.setString('tarefas', json.encode(tarefas));
          });
        }
        // Navigator.pop(context);
      },
    );
  }

  _editarTarefa({@required tarefas, @required idTarefa, @required index}) {
    TextEditingController controller =
        TextEditingController(text: tarefas[index]["tarefa"]);
    showInfo(
      label: 'Editar tarefa',
      context: context,
      textOK: 'Salvar',
      content: textFormField(
        color: colorMain,
        controller: controller,
        onPressedTextInputAction: (e) {
          if (!controller.text.isEmpty) {
            confirmEditTarefa(tarefas, idTarefa, index, controller);
          }
        },
      ),
      conformAction: () {
        if (!controller.text.isEmpty) {
          confirmEditTarefa(tarefas, idTarefa, index, controller);
        }
      },
    );
  }

  confirmEditTarefa(tarefas, idTarefa, index, controller) async {
    String ordem = await Prefs.getString('ordenacao');
    if (_pesquisa) {
      for (int i = 0; i < tarefas.length; i++) {
        if (tarefas[i]['idTarefa'] == idTarefa) {
          setState(() {
            tarefas[i]['tarefa'] = controller.text;
            _resultadoPesquisa[index]['tarefa'] = controller.text;
          });
          break;
        }
      }
    } else {
      setState(() {
        tarefas[index]['tarefa'] = controller.text;
        _resultadoPesquisa = tarefas;
      });
    }
    tarefas = ordena(
        list: tarefas,
        key: 'tarefa',
        reverse: ordem == 'crescente' ? false : true);
    Prefs.setString('tarefas', json.encode(tarefas));
  }

  actions() {
    return [
      _pesquisa
          ? Flexible(
              child: textFormField(
                  cursorColor: Colors.white,
                  controller: _pesquisaController,
                  onChanged: (search) {
                    if (_pesquisaController.text == '') {
                      setState(() {
                        _resultadoPesquisa = _tarefas;
                      });
                    }
                    var result = _tarefas.where((e) => e['tarefa']
                        .toUpperCase()
                        .contains(search.toUpperCase()));
                    result = result.toList();
                    setState(() {
                      _resultadoPesquisa = result;
                    });
                  },
                  label: 'Pesquisar',
                  suffixIcon: GestureDetector(
                    child: Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                    onTap: () {
                      _resultadoPesquisa = [];
                      setState(() {
                        _pesquisa = !_pesquisa;
                      });
                    },
                  )))
          : GestureDetector(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(Icons.search),
              ),
              onTap: () {
                _resultadoPesquisa = _tarefas;
                _pesquisaController.text = '';
                setState(() {
                  _pesquisa = !_pesquisa;
                });
              },
            ),
      _pesquisa
          ? Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: GestureDetector(
                child: Icon(Icons.clear),
                onTap: () {
                  setState(
                    () {
                      _pesquisa = !_pesquisa;
                    },
                  );
                },
              ),
            )
          : Container(),
    ];
  }

  drawer() {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text("Inicio"),
          trailing: Icon(Icons.arrow_forward),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        expansionTile(
            title: "Opções das tarefas",
            leading: Icon(Icons.assignment),
            children: [
              listTile(
                  text: 'A-Z (Ordem crescente)',
                  colorText:
                      ordemTarefas == 'crescente' ? colorMain : colorSecundary,
                  onTap: () {
                    _tarefas = ordena(list: _tarefas, key: 'tarefa');
                    Prefs.setString('ordenacao', 'crescente');
                    setState(() {
                      ordemTarefas = 'crescente';
                      Prefs.setString('tarefas', "${json.encode(_tarefas)}");
                    });
                  }),
              listTile(
                text: 'Z-A (Ordem decrescente)',
                colorText:
                    ordemTarefas == 'decrescente' ? colorMain : colorSecundary,
                onTap: () {
                  _tarefas =
                      ordena(list: _tarefas, key: 'tarefa', reverse: true);
                  Prefs.setString('ordenacao', 'decrescente');
                  setState(() {
                    ordemTarefas = 'decrescente';
                    Prefs.setString('tarefas', "${json.encode(_tarefas)}");
                  });
                },
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                child: Divider(
                  thickness: 2,
                ),
              ),
              listTile(
                text: 'Mostrar quantidade de conteúdos',
                trailing: _mostrarNumerosDaTarefa
                    ? Icon(
                        Icons.check_box,
                        color: colorMain,
                      )
                    : Icon(Icons.check_box_outline_blank),
                colorText: _mostrarNumerosDaTarefa ? colorMain : colorSecundary,
                onTap: () {
                  setState(() {
                    _mostrarNumerosDaTarefa = !_mostrarNumerosDaTarefa;
                  });
                  if (_mostrarNumerosDaTarefa) {
                    Prefs.setString('mostrarNumerosDaTarefa', 'mostrar');
                  } else {
                    Prefs.setString('mostrarNumerosDaTarefa', 'ocultar');
                  }
                },
              ),
            ]),
        expansionTile(
          leading: Icon(Icons.sort),
          title: "Opções dos conteúdos",
          children: [
            listTile(
                text: 'A-Z (Ordem crescente)',
                colorText:
                    ordemConteudos == 'crescente' ? colorMain : colorSecundary,
                onTap: () {
                  Prefs.setString('ordenacaoConteudo', 'crescente');
                  setState(() {
                    ordemConteudos = 'crescente';
                  });
                }),
            listTile(
                text: 'Z-A (Ordem decrescente)',
                colorText: ordemConteudos == 'decrescente'
                    ? colorMain
                    : colorSecundary,
                onTap: () {
                  Prefs.setString('ordenacaoConteudo', 'decrescente');
                  setState(() {
                    ordemConteudos = 'decrescente';
                  });
                }),
            listTile(
                text: 'Pendentes primeiro',
                colorText:
                    ordemConteudos == 'falsas' ? colorMain : colorSecundary,
                onTap: () {
                  Prefs.setString('ordenacaoConteudo', 'falsas');
                  setState(() {
                    ordemConteudos = 'falsas';
                  });
                }),
            listTile(
                text: 'Concluídas primeiro',
                colorText: ordemConteudos == 'verdadeiras'
                    ? colorMain
                    : colorSecundary,
                onTap: () {
                  Prefs.setString('ordenacaoConteudo', 'verdadeiras');
                  setState(() {
                    ordemConteudos = 'verdadeiras';
                  });
                }),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0),
              child: Divider(
                thickness: 2,
              ),
            ),
            listTile(
                trailing: !_mostrarConteudosConcluidos
                    ? Icon(Icons.check_box, color: colorMain)
                    : Icon(Icons.check_box_outline_blank),
                text: 'Ocultar conteudo concluído',
                colorText:
                    !_mostrarConteudosConcluidos ? colorMain : colorSecundary,
                onTap: () {
                  setState(() {
                    _mostrarConteudosConcluidos = !_mostrarConteudosConcluidos;
                    if (_mostrarConteudosConcluidos) {
                      Prefs.setString('mostrarConteudosConcluidos', 'mostrar');
                    } else {
                      Prefs.setString('mostrarConteudosConcluidos', 'ocultar');
                    }
                  });
                })
          ],
        ),
        expansionTile(
            leading: Icon(Icons.color_lens),
            title: "Cor padrao",
            children: menuDeCor(getCor)),
      ],
    );
  }
}
