import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:todo/pages/painel_informativo.dart';
import 'package:todo/utils/constants.dart';
import 'package:todo/utils/ordenacao.dart';
import 'package:todo/utils/prefs.dart';
import 'package:todo/widgets/dialod.dart';
import 'package:todo/widgets/floating_button.dart';
import 'package:todo/widgets/pop_menu_item.dart';

List _tarefas = [], _conteudos = [];
String _ordemConteudo;
bool _mostrarConcluidas = true;
int _idTarefa;
var _tarefa;
final _conteudoController = TextEditingController();

class ConteudoPage extends StatefulWidget {
  ConteudoPage({tarefas, tarefa, int idTarefa, bool conteudosMostrar}) {
    _tarefas = tarefas;
    _tarefa = tarefa;
    _idTarefa = idTarefa;
    _mostrarConcluidas = conteudosMostrar;
    _getOrdemConteudo();
  }
  @override
  _ConteudoPageState createState() => _ConteudoPageState();
}

class _ConteudoPageState extends State<ConteudoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: floatingButton(
        context: context,
        confirm: () => _confirmActionAddConteudo(),
        controller: _conteudoController,
        dialogName: 'Adicionar um conteúdo',
        cancel: () => _conteudoController.text = '',
      ),
      appBar: AppBar(
        backgroundColor: colorMain,
        title: Text('${_tarefa['tarefa']}'),
        actions: [
          popupMenuButton((e) {
            if (e == 'graficos') {
              push(
                  context,
                  Graficos(
                    checked: _tarefa['concluidas'],
                    unChecked: _tarefa['total'] - _tarefa['concluidas'],
                    total: _tarefa['total'],
                    nomeDaTarefa: _tarefa['tarefa'],
                  ));
            }
          }, [
            PopupMenuItem(
              child: Text('Gráficos'),
              value: 'graficos',
            ),
          ])
        ],
      ),
      body: _tarefa['conteudo'].length > 0
          ? _body()
          : Center(
              child: Text(
                'Nenhum conteudo cadastrado',
                style: TextStyle(fontSize: 20),
              ),
            ),
    );
  }

  _body() {
    return ListView.builder(
      padding: EdgeInsets.all(10),
      itemCount: _conteudos.length,
      itemBuilder: (context, index) {
        if (!_conteudos[index]['checked']) {
          return ListTile(
            title: Text('${_conteudos[index]['nome']}'),
            leading: Icon(
              _conteudos[index]['checked']
                  ? Icons.check_box
                  : Icons.check_box_outline_blank,
              color: colorMain,
            ),
            onTap: () {
              setState(() {
                _getOrdemConteudo();
                _conteudos[index]['checked'] = !_conteudos[index]['checked'];
              });
              atualizarNumerosTarefa();
              Prefs.setString('tarefas', '${json.encode(_tarefas)}');
            },
            onLongPress: () => _deletConteudo(
                tarefas: _tarefas,
                conteudos: _tarefa['conteudo'],
                idConteudo: index),
          );
        } else {
          return Visibility(
            visible: _mostrarConcluidas,
            child: ListTile(
              title: Text('${_conteudos[index]['nome']}'),
              leading: Icon(
                _conteudos[index]['checked']
                    ? Icons.check_box
                    : Icons.check_box_outline_blank,
                color: colorMain,
              ),
              onTap: () {
                setState(() {
                  _getOrdemConteudo();
                  _conteudos[index]['checked'] = !_conteudos[index]['checked'];
                });
                atualizarNumerosTarefa();
                Prefs.setString('tarefas', '${json.encode(_tarefas)}');
              },
              onLongPress: () => _deletConteudo(
                  tarefas: _tarefas,
                  conteudos: _tarefa['conteudo'],
                  idConteudo: index),
            ),
          );
        }
      },
    );
  }

  _confirmActionAddConteudo() {
    if (_conteudoController.text != '') {
      List tarefas = _tarefas;
      tarefas[_idTarefa]['conteudo']
          .add({"nome": "${_conteudoController.text}", "checked": false});
      setState(() {
        _getOrdemConteudo();
        atualizarNumerosTarefa();
        Prefs.setString('tarefas', "${json.encode(tarefas)}");
      });
      _conteudoController.text = '';
    }
  }

  _deletConteudo({
    tarefas,
    idConteudo,
    conteudos,
  }) {
    showInfo(
        label: 'Deletar este conteúdo',
        context: context,
        textOK: 'Deletar',
        conformAction: () {
          setState(() {
            conteudos.removeAt(idConteudo);
            tarefas[_idTarefa]['conteudo'] = conteudos;
            atualizarNumerosTarefa();
            Prefs.setString('tarefas', json.encode(tarefas));
          });
        });
  }
}

_getOrdemConteudo() async {
  _ordemConteudo = await Prefs.getString('ordenacaoConteudo');
  switch (_ordemConteudo) {
    case 'verdadeiras':
      _conteudos =
          ordena(list: _tarefa['conteudo'], key: 'checked', reverse: true);
      break;
    case 'falsas':
      _conteudos = ordena(list: _tarefa['conteudo'], key: 'checked');
      break;
    case 'crescente':
      _conteudos = ordena(list: _tarefa['conteudo'], key: 'nome');
      break;
    case 'decrescente':
      _conteudos =
          ordena(list: _tarefa['conteudo'], key: 'nome', reverse: true);
      break;
    default:
      _conteudos = ordena(list: _tarefa['conteudo'], key: 'nome');
      break;
  }
  _tarefas[_idTarefa]['conteudo'] = _conteudos;
  atualizarNumerosTarefa();
  Prefs.setString('tarefas', json.encode(_tarefas));
}

atualizarNumerosTarefa() {
  _tarefas[_idTarefa]['concluidas'] =
      contadorTarefasConluidas(lista: _tarefa['conteudo'], key: 'checked');
  _tarefas[_idTarefa]['total'] = _tarefa['conteudo'].length;
}

contadorTarefasConluidas({@required List lista, @required String key}) {
  return (lista.where((element) => element[key] == true).toList().length);
}
