import 'package:flutter/material.dart';
import 'package:flutter_circular_chart/flutter_circular_chart.dart';
import 'package:todo/utils/constants.dart';
import 'package:todo/widgets/lisi_tile.dart';

int _total, _checked, _unChecked;
String _nomeDaTarefa = '';
final _key = GlobalKey();
Color _colorPendentes = Colors.blue;

class Graficos extends StatefulWidget {
  Graficos({total, checked, unChecked, nomeDaTarefa}) {
    _total = total;
    _checked = checked;
    _unChecked = unChecked;
    _nomeDaTarefa = nomeDaTarefa;
    if (_colorPendentes == colorMain) _colorPendentes = Colors.purple;
  }
  @override
  _GraficosState createState() => _GraficosState();
}

class _GraficosState extends State<Graficos> {
  @override
  void dispose() {
    super.dispose();
    _colorPendentes = Colors.blue;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gráficos de $_nomeDaTarefa'),
        backgroundColor: colorMain,
      ),
      body: _body(),
    );
  }

  _body() {
    return ListView(
      children: [
        Visibility(
          visible: _total != 0 ? true : false,
          child: Column(
            children: [
              AnimatedCircularChart(
                key: _key,
                size: const Size(300.0, 300.0),
                initialChartData: [
                  new CircularStackEntry(<CircularSegmentEntry>[
                    CircularSegmentEntry(
                        double.parse(_checked.toString()), colorMain),
                    CircularSegmentEntry(
                        double.parse(_unChecked.toString()), _colorPendentes),
                  ])
                ],
                chartType: CircularChartType.Pie,
              ),
              listTile(
                  trailing:
                      Text('${(_checked * 100 / _total).toStringAsFixed(2)} %'),
                  text: 'Concluídas: \t\t $_checked',
                  leading: Container(
                    height: 20,
                    width: 40,
                    color: colorMain,
                  )),
              listTile(
                  trailing: Text(
                      '${(_unChecked * 100 / _total).toStringAsFixed(2)} %'),
                  text: 'Pendentes: \t\t $_unChecked',
                  leading: Container(
                    height: 20,
                    width: 40,
                    color: _colorPendentes,
                  )),
              listTile(
                text: 'Total: \t\t $_total',
                leading: Container(
                  height: 20,
                  width: 40,
                  child: Row(
                    children: [
                      Container(height: 20, width: 20, color: colorMain),
                      Container(height: 20, width: 20, color: _colorPendentes),
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
