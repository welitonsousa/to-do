import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';
import 'package:todo/utils/prefs.dart';

menuDeCor(getCor) {
  return [
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Azul',
        style: TextStyle(
            color: colorMain == Colors.blue ? colorMain : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'azul');
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Laranja',
        style: TextStyle(
            color: colorMain == Colors.deepOrangeAccent
                ? colorMain
                : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.deepOrangeAccent,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'laranja');
        print(colorMain == Colors.deepOrangeAccent);
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Verde',
        style: TextStyle(
            color: colorMain == Colors.green ? colorMain : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.green,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'verde');
        print(colorMain == Colors.green);
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Roxo',
        style: TextStyle(
            color: colorMain == Colors.purple
                ? colorMain
                : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.purple,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'roxo');
        print(colorMain == Colors.purple);
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Amarelo',
        style: TextStyle(
            color: colorMain == Colors.amber ? colorMain : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'amarelo');
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Vermelho',
        style: TextStyle(
            color: colorMain == Colors.red ? colorMain : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.red,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'vermelho');
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Marrom',
        style: TextStyle(
            color: colorMain == Colors.brown ? colorMain : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.brown,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'marrom');
        getCor();
      },
    ),
    ListTile(
      contentPadding: EdgeInsets.only(left: 30),
      title: Text(
        'Preto',
        style: TextStyle(
            color: colorMain == Colors.black ? colorMain : colorSecundary),
      ),
      leading: Container(
        height: 20,
        width: 20,
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
      onTap: () {
        Prefs.setString('cor', 'preto');
        getCor();
      },
    )
  ];
}
