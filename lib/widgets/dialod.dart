import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';

Future<bool> showInfo(
    {context,
    label,
    content,
    cancelAction(),
    conformAction(),
    textCancel = 'Cancelar',
    textOK = 'OK',
    onPressed}) {
  return showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text(
              "$label",
              style: TextStyle(color: colorMain),
            ),
          
            content: content,
            actions: <Widget>[
              FlatButton(
                child: Text(
                  '$textCancel',
                  style: TextStyle(color: colorMain),
                ),
                onPressed:() {
                  if (cancelAction != null) cancelAction();
                  Navigator.pop(context, false);
                }
              ),
              FlatButton(
                child: Text(
                  '$textOK',
                  style: TextStyle(color: colorMain),
                ),
                onPressed: (){
                  conformAction();
                  Navigator.pop(context);
                }
              ),
            ],
          ));
}
