import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';

expansionTile({
  @required String title,
  List<Widget> children,
  Widget trailing,
  Widget leading,
}) {
  return Theme(
    data: ThemeData(
      textTheme: TextTheme(
        subtitle1: TextStyle(color: colorSecundary),
      ),
      // unselectedWidgetColor: colorMain,
      dividerColor: colorMain,
      iconTheme: IconThemeData(color: colorMain),
      accentColor: colorMain,
    ),
    child: ExpansionTile(
      title: Text(title),
      leading: leading,
      trailing: trailing,
      children: children,
    ),
  );
}
