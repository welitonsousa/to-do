import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';

textFormField(
    {label,
    controller,
    onPressedTextInputAction,
    onChanged,
    suffixIcon,
    cursorColor,
    color: Colors.white}) {
  return Padding(
    padding: const EdgeInsets.only(left: 8.0),
    child: TextFormField(
      autofocus: true,
      onChanged: onChanged,
      textCapitalization: TextCapitalization.sentences,
      textInputAction: TextInputAction.done,
      controller: controller,
      style: TextStyle(color: color),
      onFieldSubmitted: onPressedTextInputAction,

      cursorColor: cursorColor!=null ? cursorColor : colorMain,
      decoration: InputDecoration(
        // suffixIcon: suffixIcon,
        border: InputBorder.none,
        labelText: label,
        labelStyle: TextStyle(
          color: Colors.white,
        ),
      ),
    ),
  );
}
