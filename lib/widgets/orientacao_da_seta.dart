import 'package:flutter/material.dart';

orientacaoDaSeta(bool estado) {
    return !estado ? Icon(Icons.arrow_drop_down) : Icon(Icons.arrow_drop_up);
  }