import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';

listTile({
  Function onTap,
  String text,
  Color colorText,
  Widget trailing,
  Widget leading,
}) {
  return ListTile(
    leading: leading,
    trailing: trailing,
    contentPadding: EdgeInsets.only(left: 30, right: 30),
    title: Text(
      text,
      style: TextStyle(
        color: colorText != null ? colorText: colorSecundary,
      ),
    ),
    onTap: onTap,
  );
}
