
import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';

import 'dialod.dart';
import 'text_form_field.dart';

floatingButton(
    {cancel, confirm, dialogName, inputName = '', controller, context, done}) {
  return FloatingActionButton(
    backgroundColor: colorMain,
    onPressed: () {
      showInfo(
        cancelAction: () => cancel(),
        conformAction: () => confirm(),
        label: '$dialogName',
        content: textFormField(
          color: colorMain,
          onPressedTextInputAction: (e) {
            confirm();
            Navigator.pop(context);
          },
          label: '$inputName',
          controller: controller,
        ),
        context: context,
      );
    },
    child: Icon(Icons.add),
  );
}
