import 'package:flutter/material.dart';
import 'package:todo/utils/constants.dart';

constrainedBox({@required String text, Function onPressed}) {
  return ConstrainedBox(
    constraints: const BoxConstraints(minWidth: double.infinity),
    child: FlatButton(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Text(
            text,
            style: TextStyle(color: colorMain, fontSize: 20.0),
          ),
        ),
        onPressed: onPressed),
  );
}
