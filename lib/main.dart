import 'package:flutter/material.dart';

import 'pages/tarefas_page.dart';
import 'utils/constants.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'To Do',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: TextTheme(
          bodyText1: TextStyle(color: colorSecundary),
          bodyText2: TextStyle(color: colorSecundary),
          headline1: TextStyle(color: colorSecundary),
          headline2: TextStyle(color: colorSecundary),
          headline3: TextStyle(color: colorSecundary),
          headline4: TextStyle(color: colorSecundary),
          subtitle1: TextStyle(color: colorSecundary),
          subtitle2: TextStyle(color: colorSecundary),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: TarefasPage()
    );
  }
}
