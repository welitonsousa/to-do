import 'package:flutter/material.dart';
import 'package:todo/utils/prefs.dart';


var cor = Prefs.getString('cor');

Color colorMain =  Colors.blue;
Color colorSecundary =  Colors.black54;

push(context, page) {
  return Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => page),
  );
}