import 'package:flutter/cupertino.dart';

ordena({@required List list,@required String key, bool reverse: false}) {
  list.sort((a, b) => a[key]
      .toString()
      .toLowerCase()
      .compareTo(b[key].toString().toLowerCase()));
  return !reverse ? list : list.reversed.toList();
}
